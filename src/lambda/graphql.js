const { ApolloServer, gql } = require("apollo-server-lambda");

const typeDefs = gql`
    type Query {
        time(request: TimeRequest): Time
        length(request: LengthRequest): Length
        mass(request: MassRequest): Mass
        thermodynamic_temperature(request: ThermodynamicTemperatureRequest): ThermodynamicTemperature
        amount_of_substance(request: AmountOfSubstanceRequest): AmountOfSubstance
        luminous_intensity(request: LuminousIntensityRequest): LuminousIntensity
    }

    input TimeRequest {
        fortnights: Float = 0
        weeks: Float = 0
        days: Float = 0
        hours: Float = 0
        minutes: Float = 0
        seconds: Float = 0
        milliseconds: Float = 0
    }

    type Time {
        fortnights: Float
        weeks: Float
        days: Float
        hours: Float
        minutes: Float
        seconds: Float
        milliseconds: Float
    }

    input LengthRequest {
        meters: Float = 0
        millimeters: Float = 0
        centimeters: Float = 0
        decimeters: Float = 0
        decameters: Float = 0
        hectometers: Float = 0
        kilometers: Float = 0
        inches: Float = 0
        feet: Float = 0
        yards: Float = 0
        miles: Float = 0
        nautical_miles: Float = 0
    }

    type Length {
        meters: Float
        millimeters: Float
        centimeters: Float
        decimeters: Float
        decameters: Float
        hectometers: Float
        kilometers: Float
        inches: Float
        feet: Float
        yards: Float
        miles: Float
        nautical_miles: Float
    }

    input MassRequest {
        kilograms: Float = 0
    }

    type Mass {
        kilograms: Float
    }

    input ThermodynamicTemperatureRequest {
        kelvins: Float = 0
    }

    type ThermodynamicTemperature {
        kelvins: Float
    }

    input AmountOfSubstanceRequest {
        moles: Float = 0
    }
    
    type AmountOfSubstance {
        moles: Float
    }

    input LuminousIntensityRequest {
        candelas: Float = 0
    }

    type LuminousIntensity {
        candelas: Float
    }
`;

const TimeDefinitions = {
    milliseconds: 0.001,
    minutes: 60,
    hours: 3600,
    days: 86400,
    weeks: 604800,
    fortnights: 1209600,
}

const LengthDefinitions = {
    millimeters: 0.001,
    centimeters: 0.01,
    decimeters: 0.1,
    decameters: 10,
    hectometers: 100,
    kilometers: 1000,
    inches: 0.0254,
    feet: 0.3048,
    yards: 0.9144,
    miles: 1609.344,
    nautical_miles: 1852,
}

const resolvers = {
  Query: {
    time: handleRequest('seconds', TimeDefinitions),
    length: handleRequest('meters', LengthDefinitions),
    mass: (_, {request}) => MassConversions(request),
    thermodynamic_temperature: (_, {request}) => TemperatureConversions(request),
    amount_of_substance: (_, {request}) => AmountOfSubstanceConversions(request),
    luminous_intensity: (_, {request}) => LuminousIntensityConversions(request),
  }
};

const handleRequest = (key, definitions) => { 
    const convert = (value, from, to) => {
        if(from === to)
            return value

        if(to === key)
            return definitions[from] * value

        if(from === key)
            return value / definitions[to]

        return convert(convert(value, from, key), key, to)
    }

    return (_, {request}) => {
        const keys = Object.keys(request)

        return Object.entries(request)
                .map(([from, value]) =>
                    keys.reduce((result, to) => {
                        let converted = convert(value, from, to)
                        result[to] = converted
                        return result
                    }, {})
                ).reduce((result, next) => {
                    Object.entries(next).forEach(([k, v]) =>
                        result[k] = result[k] ? result[k] + v : v
                    )
                    return result
                }, {})
    }
}

const MassConversions = ({kilograms}) => {
    return {
        kilograms
    }
}

const TemperatureConversions = ({kelvins}) => {
    return {
        kelvins
    }
}

const AmountOfSubstanceConversions = ({moles}) => {
    return {
        moles
    }
}

const LuminousIntensityConversions = ({candelas}) => {
    return {
        candelas
    }
}

const server = new ApolloServer({
  typeDefs,
  resolvers
});

exports.handler = server.createHandler();