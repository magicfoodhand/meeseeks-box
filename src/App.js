import React, { Component } from "react"
import "./App.css"
import { getIntrospectionQuery } from 'graphql'
import { ApolloClient, InMemoryCache } from '@apollo/client';

import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const WORKSPACES = [
  {
    name: 'Conversions',
    url: '/.netlify/functions/graphql'
  }
]

const WorkspaceDetails = ({workspace}) => {
  if(workspace === undefined)
    return <h3>Please select a workspace</h3>

  const {details, client, state, name} = workspace
  
  return <>
    <Accordion>
      <AccordionSummary
        expandIcon={<ExpandMoreIcon />}
      >
        { name }
      </AccordionSummary>
      <AccordionDetails>
        { 
          Object.entries(details).map(([queryName, typeInfo]) => {
            return <>
              <h3>{queryName}</h3>
              { Object.entries(typeInfo).map(([key, info]) => {
                return <label>
                  { key }:
                  <textarea placeholder={info.type}/>
                </label>
              })}
              <button onclick={() => console.log('Run')}>Run</button>
            </>
          })
        }
      </AccordionDetails>
    </Accordion>
  </>
}

const INTROSPECTION = getIntrospectionQuery()

const handleIntrospection = ({data}) => {
  console.log(data)
}

class App extends Component {
  constructor() {
    super()
    const workspaces = WORKSPACES.reduce((result, {name, url}, index) => {
      const details = fetch(url, {
        method: 'post', body: {
          query: INTROSPECTION,
        }
      })
      //.then(response => response.json())
      .then(handleIntrospection)
      .catch(error => {
        console.error(error)
        return {
          error, failed: true
        }
      })

      result.push({ 
        name, details, state: {},
        client: new ApolloClient({
          uri: url,
          cache: new InMemoryCache()
        }),
      })
      return result
    }, [])
    this.state = {
      currentWorkspace: undefined,
      workspaces,
    }
  }

  render() {
    const {currentWorkspace, workspaces} = this.state
    const workspace = currentWorkspace === undefined ? undefined : workspaces[currentWorkspace]
    return (
      <div className="App">
        <select onChange={({currentTarget}) => this.setState({currentWorkspace: currentTarget.value})}>
          <option>--</option>
          { 
            WORKSPACES.map(({name}, index) => 
              (<option key={'workspace-'+index} value={index}>{name}</option>))
          }
        </select>
        <WorkspaceDetails workspace={workspace} />
      </div>
    )
  }
}

export default App
